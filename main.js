//Теоретичні питання

//Чому для роботи з input не рекомендується використовувати клавіатуру?
//думаю що це повязано із тим, що просто imput буде не достатньо,
// коли ми хочемо відстежити всі події, які були зроблені користувачем
// адже існує ще й копіювання вставка за допомого миші, голосові команди
//події клавіатури краще використовувати коли ми хочемо визначити гарячі клавіші або ж
 //реагувати на клавіші зі стрілками Up та Down, комбінування клавіш

//Завдання
//Реалізувати функцію підсвічування клавіш. Завдання має бути виконане на чистому Javascript 
//без використання бібліотек типу jQuery або React.

//Технічні вимоги:

//У файлі index.html лежить розмітка для кнопок.
//Кожна кнопка містить назву клавіші на клавіатурі
//Після натискання вказаних клавіш - та кнопка, на якій написана ця літера,
 //повинна фарбуватися в синій колір. При цьому якщо якась інша літера вже раніше була
 // пофарбована в синій колір - вона стає чорною. 
 //Наприклад за натисканням Enter перша кнопка забарвлюється у синій колір. 
 //Далі, користувач натискає S, і кнопка S забарвлюється в синій колір, а кнопка Enter знову 
 //стає чорною.
 
 const listKey = document.querySelector('.btn-wrapper');

function allKey(e) {
  let elemForKey = findElementForKey(e.key.toUpperCase());
  if (!elemForKey) return;
  unpaintAllKey();
  colorKey(elemForKey);
}

function findElementForKey(keyValue) {
 return [...listKey.children].find(elem => elem.textContent.toUpperCase() === keyValue);
}

function colorKey(elemForKey) {
elemForKey.style.backgroundColor = 'blue';
}

function unpaintAllKey() {
[...listKey.children].forEach((elem) => {
elem.removeAttribute('style');
 });
}

document.addEventListener('keydown', allKey);

